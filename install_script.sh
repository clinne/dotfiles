#!/bin/bash

set -eu

echo "Starting Installation"

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
    echo "Installing homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Homebrew is already installed"
fi

# Update homebrew recipes
brew update
brew upgrade

PACKAGES=(
    bash
    ripgrep
    terraform
    neovim
    docker
    python@3.9
    python@3.10
    python3
    virtualenv
)

echo "Installing packages..."
brew list ${PACKAGES[@]} &>/dev/null || brew install ${PACKAGES[@]};

CASKS=(
    firefox
    iterm2
    TomatoBar
    jetbrains-toolbox
    keepassxc
    spotify
    obsidian
    nextcloud
    visual-studio-code
    nrlquaker-winbox
    microsoft-office
    telegram
    whatsapp
    stats
    calibre
    protonvpn
    keycastr
)

echo "Installing cask apps..."
brew list ${CASKS[@]} &>/dev/null || brew install ${CASKS[@]} --cask;

echo "Download pkg for software to install manually"
curl -o ~/Downloads/pp_vpn.pkg https://www.perfect-privacy.com/downloads/Perfect_Privacy_VPN.pkg
