#!/usr/bin/env sh

#
# for this to work you must configure sudo such that
# it will be able to run the command without password
#
# see this wiki page for information:
#  - https://github.com/koekeishiya/yabai/wiki/Installing-yabai-(latest-release)#configure-scripting-addition
#
# yabai -m signal --add event=dock_did_restart action="sudo yabai --load-sa"
# sudo yabai --load-sa
#

## Tiling options 
# layout
yabai -m config layout                       bsp
yabai -m config window_placement             second_child

# padding and gaps
yabai -m config top_padding                  12
yabai -m config bottom_padding               12
yabai -m config left_padding                 12 
yabai -m config right_padding                12
yabai -m config window_gap                   06

# split ratios
yabai -m config auto_balance                 off
yabai -m config split_ratio                  0.50

## Mouse support
yabai -m config mouse_modifier               alt #option
# set modifier + left-click drag to move window
yabai -m config mouse_action1                move
# set modifier + right-click drag to resize window
yabai -m config mouse_action2                resize
# set focus follows mouse mode (default: off, options: off, autoraise, autofocus)
yabai -m config focus_follows_mouse          autofocus
# set mouse follows focus mode (default: off)
yabai -m config mouse_follows_focus          off

## window modification
#
# floating windows are always on top (default: off)
yabai -m config window_topmost               off
# modify window shadows (default: on, options: on, off, float)
# example: show shadows only for floating windows
yabai -m config window_shadow                on
# window opacity (default: off)
# example: render all unfocused windows with 90% opacity
yabai -m config window_opacity               off
yabai -m config active_window_opacity        1.0
yabai -m config normal_window_opacity        0.90

yabai -m config debug_output on
#
# float system preferences
yabai -m rule --add app="^System Settings$" manage=off
yabai -m rule --add app='^System Information$' manage=off
#yabai -m rule --add app='^Microsoft Teams$' manage=off
yabai -m rule --add title='Preferences$' manage=off
yabai -m rule --add title='KeePassXC$' manage=off

# float settings windows
yabai -m rule --add title='Settings$' manage=off

yabai -m rule --add app="Pomotroid" manage=off
yabai -m rule --add app="^Activity Monitor$" sticky=on manage=off
yabai -m rule --add app="^Finder$" sticky=on manage=off

# Some Goland settings, in case you are using it. float Goland Preference panes
#yabai -m rule --add app='Goland IDEA' title='^$' manage=off
#yabai -m rule --add app='Goland IDEA' title='Project Structure' manage=off
#yabai -m rule --add app='Goland IDEA' title='Preferences' manage=off
#yabai -m rule --add app='Goland IDEA' title='Edit configuration' manage=off

# yabai -m config external_bar all:35:0

echo "yabai configuration loaded.."
